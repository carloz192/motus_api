//libraries
var express = require("express"),  
app = express(),
path = require('path'),
formidable = require('formidable'),
bodyParser  = require("body-parser"),
methodOverride = require("method-override"),
mongoose = require('mongoose'),
cors = require('cors'),
mysql = require('mysql');


	function base_url(client){
		return "https://"+client+"localhost/";
	}

	function site_url(client){
		return "https://"+client+"localhost/index.php/";
	}
	app.use(express.static(path.join(__dirname, 'public')));
	app.use(bodyParser.json({limit: '200mb'}));  
	app.use(bodyParser.urlencoded({ extended: false,limit: '200mb' }));  
	app.use(methodOverride());
	app.use(cors());
	


	var servers = express.Router();

	//controllers motus
	var userCtrl = require('./controllers/user');
	var mappingCtrl = require('./controllers/mapping');
	var enterpriseCtrl = require('./controllers/enterprise');
	var customerCtrl = require('./controllers/customer');
	var addressCtrl = require('./controllers/address');
	var utilitaryCtrl = require('./controllers/utilitary');

	servers.route('/login')
	.post(userCtrl.login);
	servers.route('/address_find')
	.post(addressCtrl.address_assigned);
	servers.route('/mappings')
	.post(mappingCtrl.mappingsPost);
	servers.route('/address_like')
	.post(addressCtrl.address_like);
	servers.route('/mapping')
	.post(mappingCtrl.save);
	servers.route('/image/:image')
  .get(utilitaryCtrl.image);

	app.use('/api',servers);

	app.listen(3000, function() {  
	  console.log("Node server running on http://localhost:3000");
	});

