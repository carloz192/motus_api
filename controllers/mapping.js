	var mysql = require('mysql');
	var helpers = require('../controllers/helpers');
	var crypto = require('crypto');
	var async = require("async");
	

	var client = "";
	var idworker = "";
	var arrmappingsPost = [];
	var arrmappingsGet = [];

	exports.mappingsPost =  function(req, res){
		arrmappingsPost = [];
		if(req.body && req.body.client && req.body.idworker ){
			client = req.body.client;
			idworker = req.body.idworker;
			var query = "";
			console.log("xD");
			if(req.body.format_date){

				var format_date = req.body.format_date;
				query = "SELECT m.idmappings,m.points_idpoints,DATE_FORMAT(m.start,'"+format_date+"') as start"+
				",UNIX_TIMESTAMP(date(m.start)) * 1000 as startd"+
				",DATE_FORMAT(m.end,'"+format_date+"') as end,m.type_task,m.latitude,m.longitude"+
				",DATE_FORMAT(m.date,'"+format_date+"') as date"+
				",UNIX_TIMESTAMP(date(m.date))*1000 as dated"+
				",DAYNAME(start) as nameday"+
				",DATE_FORMAT(ch.date_SP,'"+format_date+"') as date_SP"+
				",cus.name as namecus, cus.lastname as namelastcus"; 
				query += ",cus.note, ad.address, p.routes_idroutes as idroutes,es.name as statusname";
			}else{
				query = "SELECT m.idmappings,m.points_idpoints,UNIX_TIMESTAMP(m.start) * 1000 as start,UNIX_TIMESTAMP(date(m.start)) * 1000 as startd"+
				",UNIX_TIMESTAMP(m.end) *1000 as end,m.type_task,m.latitude,m.longitude,UNIX_TIMESTAMP(m.date)*1000 as date"+
				",UNIX_TIMESTAMP(date(m.date))*1000 as dated"+
				",DAYNAME(start) as nameday, UNIX_TIMESTAMP(ch.date_SP) * 1000 as date_SP"+
				",cus.name as namecus, cus.lastname as namelastcus"; 
				query += ",cus.note, ad.address, p.routes_idroutes as idroutes,es.name as statusname";
			}
				query += " FROM mappings m";
				query += " INNER JOIN customers cus ON cus.idcustomers = m.customers_idcustomers ";
				query += " INNER JOIN points p ON p.idpoints = m.points_idpoints";
				query += " INNER JOIN addresses ad ON ad.idaddresses = p.addresses_idaddresses ";
				query += " INNER JOIN states es ON es.idstates = m.states_idstates ";
				query += " LEFT JOIN checkin ch ON ch.mappings_idmappings = m.idmappings ";
				query += " WHERE workers_idworkers = "+parseInt(req.body.idworker)+" AND m.status = 1 ";
				if(req.body.start){
					query += " AND ( DATE_FORMAT(start,'%Y-%m-%d') >= '"+req.body.start+"' or DATE_FORMAT(date,'%Y-%m-%d') >= '"+req.body.start+"' )";	
				}
				if(req.body.end){
					query += " AND ( DATE_FORMAT(end,'%Y-%m-%d') <= '"+req.body.end+"' or DATE_FORMAT(date,'%Y-%m-%d') <= '"+req.body.end+"' )";	
				}
				query += " ORDER BY date desc";
				console.log(query);
				var connection = helpers.getConnection(client);
				connection.connect();
				connection.query(query,function(err,rows,fields){
					if(rows){
						if(rows.length > 0){
							async.mapSeries(rows, getCountComment, function(error,arr){
								rows.forEach(function(value,index) {
							    var tmp = {};
							    tmp = value;
							    arr.forEach(function(values,indexs){
							    	if(value.idmappings == values[0].idmappings){
							    		var obj = {"idmappings":values[0].idmappings,"count":values[0].totcomm};
							    		tmp.comment = obj;
							    	}	
							    });
							    arrmappingsPost.push(tmp);
								});

								/// se agregan otros datos, de acuerdo al cliente que sea, sino, se envia el array.
								if(client=="vppharma"){
									async.mapValues(arrmappingsPost, getDataVppharma, function(error,arr){
											var tmp = [];
											for(var index in arr) { 
												if(arr[index]){
													tmp.push(arr[index][0]);
												}
											}
											async.mapValues(tmp, getDataVppharmaAux, function(error,arrAux){
												var tmpAux = [];
												for(var index in arrAux) { 
													if(arrAux[index]){
														tmpAux.push(arrAux[index][0]);
													}
												}

												var arrTmp = arrmappingsPost;
												arrTmp.forEach(function(value,index){
													var acum = value;
													tmpAux.forEach(function(valueAux,indexAux){
														if(value.idmappings = valueAux.idmappings){
															acum.for_client = valueAux;
														}
													});
													arrmappingsPost.push(acum);
												});
												var data = formatObj(arrmappingsPost);
												res.setHeader('Content-Type', 'application/json');
												res.status(200).jsonp(data);	

											});
									});
								}else if(client=="lyp"){
									async.mapValues(arrmappingsPost, getDataLyp, function(error,arr){
										var tmp = [];
										for(var index in arr) { 
											if(arr[index]){
												tmp.push(arr[index][0]);
											}
										}

										var arrTmp = arrmappingsPost;
										arrmappingsPost = [];
										arrTmp.forEach(function(value,index){
											var acum = value;
											tmp.forEach(function(valueAux,indexAux){
												if(value.idmappings == valueAux.idmappings){
													acum.for_client = valueAux;
												}
											});
											arrmappingsPost.push(acum);
										});
										var data = formatObj(arrmappingsPost);
										res.setHeader('Content-Type', 'application/json');
										res.status(200).jsonp(data);	
									});
								}else{
									var data = formatObj(arrmappingsPost);
									res.setHeader('Content-Type', 'application/json');
									res.status(200).jsonp(data);	
								}
							});		
						}else{
							res.setHeader('Content-Type', 'application/json');
							res.status(200).jsonp(arrmappingsPost);	
						}
					}else{
						res.setHeader('Content-Type', 'application/json');
						res.status(500).jsonp({"error":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});	
					}
				});
				connection.end();
		}else{
			res.setHeader('Content-Type', 'application/json');
			res.status(400).jsonp({"error":"Sírvase proporcionar los datos requeridos."});	
		}
	}
	function formatObj(arr){
		var arrDate = [];
		//se obtiene todas las fechas posibles de las tareas
		arr.forEach(function(value,index){
			if(arrDate.indexOf(value.startd) < 0){
				arrDate.push(value.startd);
			}

			if(value.dated && value.dated != null && value.dated != undefined && arrDate.indexOf(value.dated) < 0){
				arrDate.push(value.dated);
			}

		});
		arrDate.sort(); // se ordenan las fechas por orden ascendente

		//se agrupan las tareas de acuerdo a su fecha, sea start o date
		var arrReturn = [];
		arrDate.forEach(function(value,index){
			var tmp = [];
			arr.forEach(function(values,indexs){
				if(value == values.startd || value == values.dated){
					tmp.push(values);
				}
			});
			var add = {"date":value,"mappings":tmp};
			arrReturn.push(add);
		});
		return arrReturn;
	}

	function getDataLyp(value,key, callback) {
			var connectionAux = helpers.getConnection(client);
			var query = "select "+value.idmappings+" as idmappings,ad.name as nameaddresses,en.`name` as nameentity ";
					query += " from mappings m";
					query += " inner join points p on p.idpoints = m.points_idpoints and m.idmappings = "+value.idmappings;
					query += " inner join addresses ad on ad.idaddresses = p.addresses_idaddresses";
					query += " left join mappings_entity me on me.mappings_idmappings = m.idmappings and me.entity_identity is not null ";
					query += " left join entity en on en.identity = me.entity_identity";
			connectionAux.connect();
			connectionAux.query(query,function(err,rows,fields){
				callback(null,rows);
			});
			connectionAux.end();
	}
	function getDataVppharmaAux(value,key, callback) {
			var connectionAux = helpers.getConnection(client);
			var query = "SELECT "+value.idmappings+" as idmappings,	gen.idmappingentity,gen.type,	gen.generalComment_T,	DATE_FORMAT(map.date,'%d-%m-%Y') as date";
				query += " from general gen";
				query += " inner join mappings_entity me on me.idmappingentity = gen.idmappingentity";
				query += " inner join mappings map on map.idmappings = me.mappings_idmappings";
				query += " where gen.idmappingentity = "+value.idmappingentity;
				connectionAux.connect();
				connectionAux.query(query,function(err,rows,fields){
					callback(null,rows);
				});
			connectionAux.end();
	}
	function getDataVppharma(value,key, callback) {
			var connectionAux = helpers.getConnection(client);
			var query = "select '"+value.idmappings+"' as idmappings, max(me.idmappingentity) as idmappingentity from general gen ";
			query += " inner join mappings_entity me on me.idmappingentity = gen.idmappingentity";
			query += " inner join mappings map on map.idmappings = me.mappings_idmappings";
			query += " where map.customers_idcustomers = "+value.customers_idcustomers+" and map.workers_idworkers = "+idworker;
			connectionAux.connect();
			connectionAux.query(query,function(err,rows,fields){
				callback(null,rows);
			});
			connectionAux.end();
	}
	function getCountComment(value, callback) {
		idmappings = value.idmappings;
		var data = {"idmappings":idmappings,"count":0};
		var connectionAux = helpers.getConnection(client);
		var query = "SELECT '"+idmappings+"' as idmappings,COUNT(DISTINCT ci.idcommentinvolved) as totcomm ";
		query += " from comment_involved ci";
		query += " inner join comment com on com.idcomments = ci.idcomment";
		query += " where ci.read = 0 and com.mappings_idmappings = "+idmappings+" and idworker = "+idworker;
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(null,rows);
		});
		connectionAux.end();
	}

	exports.mappingsGet = function(req,res){
		if(req.params && req.params.client && req.params.idpoints ){
			client = req.params.client;
			var query = " SELECT idmappings, workers_idworkers, points_idpoints, start, end, cus.name as namecus, ";
			query += " cus.lastname as namelastcus, ad.address, p.routes_idroutes as idroutes, m.comment,";
			query += " title, editable, m.latitude, m.longitude, m.date, m.states_idstates, m.customers_idcustomers, ";
			query += " DAYNAME(m.start) as nameday";
			query += " FROM (mappings m)";
			query += " INNER JOIN customers cus ON cus.idcustomers = m.customers_idcustomers";
			query += " INNER JOIN points p ON p.idpoints = m.points_idpoints";
			query += " INNER JOIN addresses ad ON ad.idaddresses = p.addresses_idaddresses";
			query += " WHERE m.points_idpoints =  '"+req.params.idpoints+"'";
			query += " AND m.states_idstates IN (1, 3) ";
			query += " ORDER BY m.date asc, m.idmappings desc";
				var connection = helpers.getConnection(client);
				connection.connect();
				connection.query(query,function(err,rows,fields){
					if(rows){
						res.setHeader('Content-Type', 'application/json');
						res.status(200).jsonp(rows);
					}else{
						res.setHeader('Content-Type', 'application/json');
						res.status(500).jsonp({"error":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});	
					}
				});
		}else{
			res.setHeader('Content-Type', 'application/json');
			res.status(400).jsonp({"error":"Sírvase proporcionar los datos requeridos."});	
		}
	}


	exports.save =  function(req, res){
		arrmappingsPost = [];
		client = req.body.client;
		if(req.body && req.body.start && req.body.start.length > 0  ){
			var adate = req.body.start.split(" ")[0].split("-");;
			var date = adate[2]+"-"+adate[1]+"-"+adate[0];
			idworkers = req.body.idworkers;
			var query = "select * from routes where trim(name) = trim('"+date+"') and creator = "+idworkers+" limit 1";

			var connection = helpers.getConnection(client);
			connection.connect();
			connection.query(query,function(err,rows,fields){
				if(rows){
					var arr = [req.body];
					if(rows.length > 0){
						arr[0].idroute = rows[0].idroutes; 

						async.mapSeries(arr, pointcheck, function(errorcp,rptacp){ // se comprueba si ya esta creado un punto
							
							if(rptacp.length > 0 && rptacp[0].length > 0 ){ // el punto esta creado
								arr[0].idpoint = rptacp[0][0].idpoints; // se agrega al array el id de la ruta

								async.mapSeries(arr, mappingcreate, function(errorm,rptam){ // se crea la tarea
									var idmapping = rptam[0].insertId;
									arr[0].idmapping = idmapping;

										if( arr[0].checkin && arr[0].checkin.length > 0 ){// si se ha hecho checkin entonces se inserta
												async.mapSeries(arr, checkincreate, function(errorm,rptam){ // se crea la tarea
												});
										}
										
										// if( arr[0] && arr[0]['forms[terreno][comment_T]'] ){
											async.mapSeries(arr, mappingentitycreate, function(errorme,rptame){ // se crea la entidad de la tarea
													//ccarlos
													var idmapping = arr[0].idmapping;
													var data = {"rpta":true,"id":idmapping,"mensaje":"datos guardados correctamente"};
													res.setHeader('Content-Type', 'application/json');
													res.status(200).jsonp(data);	
											});

								});

							}else{

								async.mapSeries(arr, pointcreate, function(errorp,rptap){ // se crea el punto
									
									var idpoint = rptap[0].insertId;
									arr[0].idpoint = idpoint; // se agrega al array el id de la ruta

									async.mapSeries(arr, mappingcreate, function(errorm,rptam){ // se crea la tarea
										var idmapping = rptam[0].insertId;
										arr[0].idmapping = idmapping;


										if( arr[0].checkin && arr[0].checkin.length > 0 ){// si se ha hecho checkin entonces se inserta
												async.mapSeries(arr, checkincreate, function(errorm,rptam){ // se crea la tarea
												});
										}


											async.mapSeries(arr, mappingentitycreate, function(errorme,rptame){ // se crea la entidad de la tarea
													//ccarlos
													var idmapping = arr[0].idmapping;
													var data = {"rpta":true,"id":idmapping,"mensaje":"datos guardados correctamente"};
													res.setHeader('Content-Type', 'application/json');
													res.status(200).jsonp(data);	
											});

									});
								});
							}
						});

					}else{ // no hay ruta para esta fecha de este trabajador
						
						async.mapSeries(arr, routecreate, function(error,rpta){// se crea la tarea
								var idroute = rpta[0].insertId;
								arr[0].idroute = idroute; // se agrega al array el id de la ruta
							
								async.mapSeries(arr, pointcreate, function(errorp,rptap){ // se crea el punto
									var idpoint = rptap[0].insertId;
									arr[0].idpoint = idpoint; // se agrega al array el id de la ruta

											async.mapSeries(arr, mappingcreate, function(errorm,rptam){ // se crea la tarea
												var idmapping = rptam[0].insertId;
												arr[0].idmapping = idmapping;

															if( arr[0].checkin && arr[0].checkin.length > 0 ){// si se ha hecho checkin entonces se inserta
																	async.mapSeries(arr, checkincreate, function(errorm,rptam){ // se crea la tarea
																	});
															}


																async.mapSeries(arr, mappingentitycreate, function(errorme,rptame){ // se crea la entidad de la tarea
																		//ccarlos
																		var idmapping = arr[0].idmapping;
																		var data = {"rpta":true,"id":idmapping,"mensaje":"datos guardados correctamente"};
																		res.setHeader('Content-Type', 'application/json');
																		res.status(200).jsonp(data);	
																});
														

											});
								});
						});
					}
				}else{ // cuando hay algún problema de conecion
					res.setHeader('Content-Type', 'application/json');
					res.status(500).jsonp({"error":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});	
				}
			});
			connection.end();
		}else{
			res.setHeader('Content-Type', 'application/json');
			res.status(400).jsonp({"error":"Sírvase proporcionar los datos requeridos."});	
		}
	}

	function formcheck(value,callback) {
		var idworkers = value.idworkers;
		var idroute = value.idroute;
		var idaddress = value.idaddress;
		var query = "SELECT * from points where addresses_idaddresses = "+idaddress+" and routes_idroutes = "+idroute+" and responsible = "+idworkers+ " limit 1";

		var connectionAux = helpers.getConnection(client);
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(err,rows);
		});
		connectionAux.end();
	}

	function pointcheck(value,callback) {

		var idworkers = value.idworkers;
		var idroute = value.idroute;
		var idaddress = value.idaddress;


		var query = "SELECT * from points where addresses_idaddresses = "+idaddress+" and routes_idroutes = "+idroute+" and responsible = "+idworkers+ " limit 1";

		var connectionAux = helpers.getConnection(client);
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(err,rows);
		});
		connectionAux.end();
	}

	function routecreate(value,callback) {
		var connectionAux = helpers.getConnection(client);
		var adate = value.start.split(" ")[0].split("-");
		var date = adate[2]+"-"+adate[1]+"-"+adate[0];
		var datemysql = value.start.substring(0, 10);
		var idworkers = value.idworkers;

		var query = "insert into routes (name,startDate,endDate,comment,states_idstates,creator) values "+
		"('"+date+"','"+datemysql+" 00:00:00','"+datemysql+" 23:59:59','',1,"+idworkers+")";
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(null,rows);
		});
		connectionAux.end();
	}

	function pointcreate(value,callback) {

		var idworkers = value.idworkers;
		var idroute = value.idroute;
		var idaddress = value.idaddress;


		var query = "insert into points (addresses_idaddresses,routes_idroutes,responsible,states_idstates,comment) values "+
		"("+idaddress+","+idroute+","+idworkers+",1,'')";
		var connectionAux = helpers.getConnection(client);
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(err,rows);
		});
		connectionAux.end();
	}

	function mappingcreate(value,callback) {
		var idworkers = value.idworkers;
		var idpoint = value.idpoint;
		var start = value.start;
		var end = value.start.substring(0, 10);
		var states_idstates = value.checkout ? 2 : 1;
		var latitudeout = value.latitudeout ? value.latitudeout : '';
		var longitudeout = value.longitudeout ? value.longitudeout : '';
		var checkout = value.checkout ? value.checkout : '0000-00-00 00:00:00';
		var idcustomers = value.idcustomer ? value.idcustomer : 0;


		var query = "insert into mappings (workers_idworkers,points_idpoints,start,end,states_idstates,type_task,latitude,longitude,date,customers_idcustomers,status) values "+
		"("+idworkers+","+idpoint+",'"+start+"','"+end+"',"+states_idstates+",2,'"+latitudeout+"','"+longitudeout+"','"+checkout+"',"+idcustomers+",1)";
		var connectionAux = helpers.getConnection(client);
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(err,rows);
		});
		connectionAux.end();
	}

	function mappingentitycreate(value,callback) {
		var idmapping = value.idmapping;
		var query = "insert into mappings_entity (mappings_idmappings) values "+
		"("+idmapping+")";
		var connectionAux = helpers.getConnection(client);
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			// callback(err,rows);
			var arr = [value];
			arr[0].idmappingentity = rows.insertId;
			arr[0].callback = callback;
				
					async.mapSeries(arr, mappingformentitycreate, function(errormfe,rptamfe){ // se crea la entidad de la tarea
							arr[0].idmappformenti = rptamfe.insertId;
							async.mapSeries(arr, mappingformentitystatuscreate, function(errormfes,rptamfes){ // se crea la entidad de la tarea
							});
							async.mapSeries(arr, formcreate, function(errormfes,rptamfes){ // se crea la entidad de la tarea
									arr[0].callback(err,arr);
							});
					});
		});
		connectionAux.end();
	}

	function mappingformentitycreate(value,callback) {
		var idmappingentity = value.idmappingentity;

		var query = "insert into mapping_form_entity (idmappingentity,form_idform) values "+
		"("+idmappingentity+",1)";
		var connectionAux = helpers.getConnection(client);
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(err,rows);
		});
		connectionAux.end();
	}

	function mappingformentitystatuscreate(value,callback) {

		var idmappformenti = value.idmappformenti;
		var start = value.start;
		var idworkers = value.idworkers;

		var query = "insert into mappformenti_status (idmappformenti,status_idstatus,date,workers_idworkers,comment) values "+
		"("+idmappformenti+",1,'"+start+"',"+idworkers+",'------')";
		var connectionAux = helpers.getConnection(client);
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(err,rows);
		});
		connectionAux.end();
	}


	function formcreate(value,callback) {

		var idmappingentity = value.idmappingentity;
		var client = value.client;
		var comment = value.comment ? value.comment : "";

		if(value.image && value.image.length > 0){

			helpers.createimage(value.image,client,function(error,response){
				image = response.name;
				var query = "insert into terreno (comment_T,file1_F,type,idmappingentity) values "+
				"('"+comment+"','"+image+"',1,"+idmappingentity+")";
				var connectionAux = helpers.getConnection(client);
				connectionAux.connect();
				connectionAux.query(query,function(err,rows,fields){
					callback(err,rows);
				});
				connectionAux.end();

			})

		}else{

			helpers.createimage(value.image,client,function(error,response){
				image = response.name;
				var query = "insert into terreno (comment_T,file1_F,type,idmappingentity) values "+
				"('"+comment+"','',1,"+idmappingentity+")";
				var connectionAux = helpers.getConnection(client);
				connectionAux.connect();
				connectionAux.query(query,function(err,rows,fields){
					callback(err,rows);
				});
				connectionAux.end();

			})

		}
	}

	function checkincreate(value,callback) {

		var idmapping = value.idmapping;
		var checkin = value.checkin ? value.checkin : '';
		var latitude = value.latitude ? value.latitude : '';
		var longitude = value.longitude ? value.longitude : '';


		var query = "insert into checkin (mappings_idmappings,date_SP,latitude,longitude,checkinStatus_B) values "+
		"("+idmapping+",'"+checkin+"','"+latitude+"','"+longitude+"',1)";
		var connectionAux = helpers.getConnection(client);
		connectionAux.connect();
		connectionAux.query(query,function(err,rows,fields){
			callback(err,rows);
		});
		connectionAux.end();
	}