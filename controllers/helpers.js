	var mysql = require('mysql');
	var fs = require('fs');
	var sendgrid = require('sendgrid').mail;
	var sg = require('sendgrid')("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");

	exports.makeid = function(){
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	    for( var i=0; i < 30; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    return text;
	};

	exports.getConnection = function(database,alone){
		database = database ? database : "lyp";
		database = !alone ? "motus_"+database+"_core" : database;
		console.log(database);
		var data = {};
		var connection = mysql.createConnection({
			host:'localhost',
			user:'root',
			//password:'',
			password:'31340140',
			database:database
		});
		return connection;
	}


	exports.sendemail = function(data,callback){
		
	  var title = data.title ? data.title : "Aviso motus";

	  var from_email = data.from_email ? data.from_email : "notifications@motusgl.com";
	  from_email = new sendgrid.Email(from_email);

	  var to_email = new sendgrid.Email(data.to_email);
	  var content = new sendgrid.Content('text/html', data.body);

	  var mail = new sendgrid.Mail(from_email, title, to_email, content);

	  if(data.cc && data.cc.length > 0){ //ADD CC
	  	var cc = data.cc;
	  	cc.forEach(function(value,index){
				mail.personalizations[0].addCc( new sendgrid.Email(value) ) ;
	  	});
	  }

	  if(data.bcc && data.bcc.length > 0){ //ADD BCC
	  	var bcc = data.bcc;
	  	bcc.forEach(function(value,index){
				mail.personalizations[0].addBcc( new sendgrid.Email(value) ) ;
	  	});
	  }

	  var request = sg.emptyRequest({
	    method: 'POST',
	    path: '/v3/mail/send',
	    body: mail.toJSON()
	  });
	  sg.API(request, function(error, response) {
	  	var rpta = {"email":data.to_email};
	  	console.log("enviado a "+data.to_email);
	  	if(callback){
	  		callback(error,rpta);
	  	}
	  });

	}

exports.createimage = function(base64Data,client,callback){
	var name = randonString()+".png";
	var retur = {"name":name};
	var patch = "./../motus/img/uploads/"+client+"/"+name;
	fs.writeFile(patch, base64Data, 'base64', function(error) {
		callback(error,retur);
	});
}

function randonString(max) {
	max = max ? max : 20;
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < max; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text + new Date().getTime().toString();
}