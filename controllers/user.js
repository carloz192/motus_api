	var mysql = require('mysql');
	var helpers = require('../controllers/helpers');
	var crypto = require('crypto');

	exports.login =  function(req, res){
		var data = {};
		console.log(":p");
		console.log(req.body);
		if(req.body && req.body.username && req.body.password && req.body.client ){ // PARA LOGIN
			var connection = helpers.getConnection(req.body.client);
			connection.connect();
			var hash = crypto.createHash('sha1').update(req.body.password).digest("hex");
			console.log(hash);
			// var hash = req.body.password;
			//prueba coneccion a bd
			var query = "select idworkers,name,lastname,email,pass,clients_idclients,phone,cellphone,code,position,proportional,goal,status,note,UNIX_TIMESTAMP(workers.createdate)*1000 as createdate,UNIX_TIMESTAMP(workers.enddate)*1000 as enddate,img from workers where email = '"+req.body.username+"' limit 1";
			connection.query(query,function(err,rows,fields){
				console.log(err);
				if(rows){
					if(rows.length > 0){
						if(rows[0].pass == hash){
							res.setHeader('Content-Type', 'application/json');
							res.status(200).jsonp(rows[0]);
						}else{
							res.setHeader('Content-Type', 'application/json');
							res.status(401).jsonp({"error":"correo electrónico o contraseña incorrecta."});
						}
					}else{
						res.setHeader('Content-Type', 'application/json');
						res.status(401).jsonp({"error":"correo electrónico o contraseña incorrecta."});
					}
				}else{ /// si no se conecto a la bd
					res.setHeader('Content-Type', 'application/json');
					res.status(500).jsonp({"error":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});
				}
			});
			connection.end();
		}else{
			if(!req.body){
				res.setHeader('Content-Type', 'application/json');
				res.status(400).jsonp({"error":"Sírvase proporcionar correo electrónico"});				
			}else if(!req.body.username){
				res.setHeader('Content-Type', 'application/json');
				res.status(400).jsonp({"error":"Sírvase proporcionar correo electrónico"});			
			}else if(!req.body.password){
				res.setHeader('Content-Type', 'application/json');
				res.status(400).jsonp({"error":"Sírvase proporcionar contraseña"});			
			}else if(!req.body.client){
				res.setHeader('Content-Type', 'application/json');
				res.status(400).jsonp({"error":"Sírvase proporcionar nombre de empresa"});			
			}
		} 
	}

	exports.recoverpass = function(req, res){
		if(req.body.email && req.body.client){ // PARA RECUPERAR EL PASSWORD
			var query = "select * from workers where email = '"+req.body.email+"' limit 1";
			var connection = helpers.getConnection(req.body.client);
			connection.query(query,function(err,rows,fields){
				if(rows){
					if(rows.length > 0){
						data = rows[0];
						var hash = helpers.makeid();
						var query = "insert into recoverpass(workers_idworkers,hash,dateExp) values('"+data.idworkers+"','"+hash+"','2016-08-08 00:00:00')";
						var connection = helpers.getConnection(req.body.client);
						connection.query(query,function(errs,rowss,fieldss){
							if(rowss && rowss.insertId){
								var bodyMail = "<!DOCTYPE html><html lang='en'><head><meta charset='utf-8'></head><body role='document'><table cellspacing='0' cellpadding='0' style='border-collapse:collapse;width:98%' border='0'><tbody><tr><td style='font-family:lucida grande,tahoma,verdana,arial,sans-serif;font-size:12px;padding:0px;background:#e0e1e5'><table cellspacing='0' cellpadding='0' width='100%' border='0' style='border-collapse:collapse;width:100%'><tbody><tr><td style='font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:0;border-left:none;border-right:none;border-top:none;border-bottom:none'><table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:collapse'><tbody> <tr> <td style='padding:0;width:100%'><table cellspacing='0' cellpadding='0' width='100%' bgcolor='#363636' style='border-collapse:collapse;width:100%;background:#363636' style='line-height:7px'><tbody><tr><td><center><table cellspacing='0' cellpadding='0' width='610' height='44' style='border-collapse:collapse'><tbody><tr><td align='left' style='width:100%;line-height:47px'><table cellspacing='0' cellpadding='0' style='border-collapse:collapse'><tbody><tr><td width='19'><a href='' style='color:#3b5998;text-decoration:none' target='_blank'><img src='http://www.motusgl.com/es/images/logo.png' style='border:0;display:block' class='CToWUd'><!-- Logo empresa--></a></td></tr></tbody></table></td></tr></tbody></table></center></td></tr></tbody></table></td></tr><tr><td style='padding:0;width:100%'><table cellspacing='0' cellpadding='0' width='100%' bgcolor='#e0e1e5' style='border-collapse:collapse'><tbody><tr><td><table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:collapse'><tbody><tr><td height='19'>  &nbsp;   </td></tr></tbody></table> <center> <table cellspacing='0' cellpadding='0' width='610' style='border-collapse:collapse'><tbody><tr><td align='left' style='background-color:#ffffff;border-color:#c1c2c4;border-style:solid;display:block;border-width:1px;border-radius:5px;overflow:hidden'><table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:collapse'><tbody><tr>  <td style='padding:15px'><table cellspacing='0' cellpadding='0' style='border-collapse:collapse;width:100%'><tbody><tr><td style='font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-bottom:6px'><div>Para recuperar tu contraseña de motus, pincha el siguiente enlace: </div> <a href='";
								bodyMail += "https://"+req.body.client+".motusgl.com/login/changePass/"+hash+"' style='color:#3b5998;text-decoration:none' target='_blank'></a></td></tr><tr></tr><tr> <td style='font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-top:6px'><a href='' style='color:#3b5998;text-decoration:none' target='_blank'><table cellspacing='0' cellpadding='0' width='100%' bgcolor='#4c649b' style='border-collapse:collapse;border-width:1px;border-style:solid;display:block;font-weight:bold;border-radius:3px;font-size:14px;border-color:#485a83;text-align:center'><tbody><tr> <td height='7' colspan='3' style='line-height:7px'></td> </tr><tr><td width='16' style='display:block;width:16px'>&nbsp;</td><td width='100%' style='text-align:center'><a href='";
								bodyMail += "https://"+req.body.client+".motusgl.com/login/changePass/"+hash+"' style='color:#3b5998;text-decoration:none;display:block' target='_blank'><center> <font size='3'><span style='font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif;font-weight:bold;font-size:14px;color:#ffffff'>Cambiar&nbsp;la&nbsp;contraseña</span></font></center></a></td><td width='16' style='display:block;width:16px'>&nbsp;</td></tr><tr><td height='7' colspan='3' style='line-height:7px'>&nbsp;</td></tr></tbody></table></a></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></center></td></tr></tbody></table></td></tr><tr><td style='padding:0;width:100%'><table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:collapse'><tbody><tr><td><center><table cellspacing='0' cellpadding='0' width='610' style='border-collapse:collapse'><tbody><tr><td><table cellspacing='0' cellpadding='0' width='610' border='0' style='border-collapse:collapse'><tbody><tr><td style='font-size:12px;font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif;padding:18px 0;border-left:none;border-right:none;border-top:none;border-bottom:none;color:#6a7180;font-weight:300;line-height:16px;text-align:center;border:none'>Se envió este mensaje a petición tuya. Getkem S.A., Attention: Almirante Pastene 0000, Providencia Santiago de Chile </td></tr></tbody></table></td></tr></tbody></table></center></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></body></html>";
							  var helper = require('sendgrid').mail;
							  var from_email = new helper.Email('no-reply@getkem.com');
							  var to_email = new helper.Email(req.body.email);
							  var subject = 'Recuperar Contraseña';
							  var content = new helper.Content('text/html', bodyMail);
							  var mail = new helper.Mail(from_email, subject, to_email, content);
							  var sg = require('sendgrid')("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");
							  var request = sg.emptyRequest({
							    method: 'POST',
							    path: '/v3/mail/send',
							    body: mail.toJSON()
							  });
							  sg.API(request, function(error, response) {
							  	if(error){
										res.setHeader('Content-Type', 'application/json');
										res.status(500).jsonp({"error":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});	
							  	}else{
										res.setHeader('Content-Type', 'application/json');
										res.status(200).jsonp(true);	
							  	}
							  });
							}
						});
					}else{
						res.setHeader('Content-Type', 'application/json');
						res.status(400).jsonp({"error":"Ingrese por favor correo electrónico válido."});
					}
				}else{ // si no arroja ningún resultado es por que algo paso en la bd
					res.setHeader('Content-Type', 'application/json');
					res.status(400).jsonp({"error":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});
				}
			});
			connection.end();
		}else{
			res.setHeader('Content-Type', 'application/json');
			res.status(400).jsonp({"error":"Sírvase proporcionar correo electrónico"});
		}
	}