	var mysql = require('mysql');
	var helpers = require('../controllers/helpers');
	var async = require("async");

	exports.customers = function(req,res){
		arrmappingsPost = [];
		if(req.params && req.params.client){
			client = req.params.client;
			var query = "SELECT cus.idcustomers,cus.name,cus.lastname,cus.phone,cus.cellphone,cus.email,cus.note,cus.`code`,cus.clients_idclients"+
			",ad.idaddresses,ad.address,ad.reference,ad.customers_idcustomers,ad.commune_idcommune,ad.name as aname,ad.latitude,ad.longitude"+
			",ad.contactName,ad.contactCellphone,ad.contactPhone,ad.contactEmail,ad.`status`"+
			" from customers cus "+
			" inner join addresses ad on ad.customers_idcustomers = cus.idcustomers"+
			" order by cus.`name`,cus.lastname,ad.name";
				var connection = helpers.getConnection(client);
				connection.connect();
				connection.query(query,function(err,rows,fields){
					if(rows){
						if(rows.length > 0){

							var data = formatCustomer(rows);
							res.setHeader('Content-Type', 'application/json');
							res.status(200).jsonp(data);	
							
						}else{
							res.setHeader('Content-Type', 'application/json');
							res.status(200).jsonp(rows);	
						}
					}else{
						res.setHeader('Content-Type', 'application/json');
						res.status(500).jsonp({"error":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});	
					}
				});
				connection.end();
		}else{
			res.setHeader('Content-Type', 'application/json');
			res.status(400).jsonp({"error":"Sírvase proporcionar los datos requeridos."});	
		}
	}

	function formatCustomer(arr){
		var arrReturn = [];
		var idcustomers = 0;
		var indice = -1;
		arr.forEach(function(values,index){
			
			if(idcustomers != values.idcustomers){
				idcustomers = values.idcustomers;
				indice++;
				var address = {"idaddresses":values.idaddresses,"address":values.address,"reference":values.reference,
				"commune_idcommune":values.commune_idcommune,"name":values.aname,"latitude":values.latitude,"longitude":values.longitude,
				"contactName":values.contactName,"contactCellphone":values.contactCellphone,"contactPhone":values.contactPhone,
				"contactEmail":values.contactEmail};
				var addresses = [];
				addresses.push(address);
				var customers = {"idcustomers":values.idcustomers,"name":values.name,"lastname":values.lastname,"phone":values.phone,"cellphone":values.cellphone,
				"email":values.email,"note":values.note,"code":values.code,"addresses":addresses};
				arrReturn[indice] = customers;
				
			}else{
				var address = {"idaddresses":values.idaddresses,"address":values.address,"reference":values.reference,
				"commune_idcommune":values.commune_idcommune,"name":values.aname,"latitude":values.latitude,"longitude":values.longitude,
				"contactName":values.contactName,"contactCellphone":values.contactCellphone,"contactPhone":values.contactPhone,
				"contactEmail":values.contactEmail};
				var arrAdd = arrReturn[indice].addresses;
				arrAdd.push(address);

				arrReturn[indice].addresses = arrAdd ;
			}
		});
		return arrReturn;
	}

	exports.save = function(req,res){
		arrmappingsPost = [];
		client = "megarep";
		if(req.body && req.body.name && req.body.name.length > 0){
			var arr = req.body;
			var arrnum = [1,2,3,4,5,6,7,8,9];
			var query = "";
			var idcustomers = arr.idcustomers;
			if( idcustomers && arrnum.indexOf( parseInt(idcustomers.toString().substring(0,1)) ) >= 0 ){
				query = "update customers set name='"+arr.name+"',lastname='"+arr.lastname+"',phone='"+arr.phone+
						"',cellphone='"+arr.cellphone+"',email='"+arr.email+"',note='"+arr.note+"' where idcustomers = "+idcustomers;
			}else{
				query = "insert into customers (code,name,lastname,phone,cellphone,email,note) values"+
				"('"+arr.code+"','"+arr.name+"','"+arr.lastname+"','"+arr.phone+"','"+arr.cellphone+"','"+arr.email+"','"+arr.note+"')";
			}

				var connection = helpers.getConnection(client);
				connection.connect();
				connection.query(query,function(err,rows,fields){
					if(rows){
						var id = rows.insertId != 0 ? rows.insertId : idcustomers;
						var data = {"rpta":true,"id":id,"msg":"Datos guardados correctamente"};
						res.setHeader('Content-Type', 'application/json');
						res.status(200).jsonp(data);	
					}else{
						res.setHeader('Content-Type', 'application/json');
						res.status(500).jsonp({"msg":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});	
					}
				});
				connection.end();
		}else{
			res.setHeader('Content-Type', 'application/json');
			res.status(400).jsonp({"msg":"Sírvase proporcionar los datos requeridos."});	
		}
	}