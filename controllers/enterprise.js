	var mysql = require('mysql');
	var helpers = require('../controllers/helpers');
	var async = require("async");

	exports.enterprises = function(req,res){
		var query = "SELECT idclients,socialName from clients";
			var connection = helpers.getConnection("motus_core",true);
			connection.connect();
			connection.query(query,function(err,rows,fields){
				if(rows){
					res.setHeader('Content-Type', 'application/json');
					res.status(200).jsonp(rows);
				}else{
					res.setHeader('Content-Type', 'application/json');
					res.status(500).jsonp({"error":"Algo salió mal. Por favor, inténtelo de nuevo más tarde."});	
				}
			});
	}

	exports.enterprise = function(req,res){
		console.log(req.params);
		if(req.params && req.params.id ){
			var query = "SELECT idclients,socialName from clients where idclients = "+parseInt(req.params.id);
			console.log(query);
				var connection = helpers.getConnection("motus_core",true);
				connection.connect();
				connection.query(query,function(err,rows,fields){
					console.log(rows);
					if(rows[0]){
						res.setHeader('Content-Type', 'application/json');
						res.status(200).jsonp(rows[0]);
					}else{
						res.setHeader('Content-Type', 'application/json');
						res.status(500).jsonp({"error":"Código no existe, preguntar el correcto al proveedor."});	
					}
				});
		}else{
			res.setHeader('Content-Type', 'application/json');
			res.status(400).jsonp({"error":"Sírvase proporcionar los datos requeridos."});	
		}
	}