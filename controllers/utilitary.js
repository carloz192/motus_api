var fs = require('fs');

exports.image = function(req,res){
	var image;
	var img;
	try {
		var image = req.params.image;
		var img;
		img = fs.readFileSync('/var/www/html/motus/img/uploads/velax/'+image);
	}catch (error) {
		img = fs.readFileSync('/var/www/html/motus/img/uploads/velax/image.png');
	}
	res.writeHead(200, {'Content-Type': 'image/png' });
	res.end(img, 'binary');
}