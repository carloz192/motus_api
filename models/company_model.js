	var mysql = require('mysql');
	var helpers = require('../controllers/helpers');
	var async = require("async");
	var dbname = "motusc";


	exports.usercompany =  function(data,callback){
		if(data.iduser){ // LISTAR NOTAS
			var iduser = data.iduser;
			var query = "SELECT company_idcompany from user_company where user_iduser = "+iduser;
				
			var connection = helpers.getConnection(dbname,true);
			connection.connect();
			connection.query(query,function(err,rows,fields){
				callback(err,rows);
			});
			connection.end();
		}else{
			callback(true,null);	
		} 
	}


	exports.findbyid =  function(data, callback){
		if(data.idcompany){ // LISTAR NOTAS
			var idcompany = data.idcompany;
			var query = "SELECT * from company where idcompany = "+idcompany;
			var connection = helpers.getConnection(dbname,true);
			connection.connect();
			connection.query(query,function(err,rows,fields){
				callback(err,rows[0]);
			});
			connection.end();
		}else{
			callback(true,null);	
		} 
	}


	exports.findallcbo =  function(data, callback){
		if( data.indexcbo && data.tablename ){ // LISTAR NOTAS
			var indexcbo = data.indexcbo;
			var tablename = data.tablename;
			var query = "SELECT cd.* from "+tablename+" cp "+
			" left join "+tablename+"_detail cd on cd.index = cp.id"+
			" where cp.status = 1 and cd.status = 1 and cp.id = "+indexcbo;
			console.log(query);
			var connection = helpers.getConnection(dbname,true);
			connection.connect();
			connection.query(query,function(err,rows,fields){
				callback(err,rows);
			});
			connection.end();
		}else{
			callback(true,null);	
		} 
	}